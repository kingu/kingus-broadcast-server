#!/usr/local/bin/gosh

(use kingu.taffy)
;(use kingu.mpd)
(use kingu.mpc)
(use kingu.file)
(use kingu.content-type)

(define main
  (lambda (args)
    (let* ((tag (cadr args))
	   (value (caddr args))
	   (dir (cadddr args))
	   (files (recursive-directory-list dir)))

      (for-each
       (lambda (x)
	 ;(write (car (content-type-of x)))
	 (when (string=? "audio" (car (content-type-of x)))
	       (begin
		 (print x)
		 (taffy-write! x `((,tag ,value))))))
       files)
;      (mpc-update)
      )))

 
