#!/usr/local/bin/gosh

(use kingu.mpc)


(define is-playlist-empty?
  (lambda  ()
    (null? (mpc 'playlist))))

(define main
  (lambda (args)
    
    (when (is-playlist-empty?)
	  (mpc 'add "Chara/Montage/My Way.mp3"))
	  
    (unless (is-mpd-playing?)
	    (mpc 'play))))

