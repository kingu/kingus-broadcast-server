#!/usr/local/bin/gosh

(use gauche.process)
(use kingu.process)
(use kingu.file)
(use kingu.string)
(use kingu.mpc)
(use kingu.sync)
(use file.util)
(use srfi-13)
(use rfc.uri)
(use gauche.collection)
(use text.progress) 

;(load "/home/grpmpdp/lib/libgrpmpdp.scm")

(define target-dir "/tmp/ds/")

(define (num-format cur max)
    (format "~d/~d(~3d%)" cur max
            (round->exact (/. (* cur 100) max))))

(define sortable-bandname
  (lambda (bn)
    (remove-leading-article (remove-from-string (remove-diacritic-from-string bn) #\" #\/ #\\))))

(define main
  (lambda (args)
    (print "Updating the MPD database")
    (mpc-update)    
    (print "Creating temporary working directory")
    (unless (file-exists? target-dir) (create-directory* target-dir))
    
    (let* ((raw-artist-list (mpc 'list 'artist))
	   (classical-artist-list (mpc 'list 'artist 'genre "Classical"))
	   (classical-composer-list (mpc 'list 'composer 'genre "Classical"))
	   (artist-list (append (filter (lambda (x)
					  (not (or (and (member x classical-artist-list string=?)
							(= 1 (length (mpc 'list 'genre 'artist x)))) 
						   (string=? x ""))))
					raw-artist-list)
				(filter (lambda (x) (not (string=? x "")))
					classical-composer-list)))

	   (p (make-text-progress-bar :header "Jukebox"
				      :header-width 10
				      :bar-char #\☵
				      :info-width 30
				      :num-width 13
				      :num-format num-format
				      :max-value (length artist-list))) )
      
      (call-with-output-file (string-append target-dir "artists.html")	
	(lambda (artist-p)
	  (for-each
	   (lambda (a)
;;	     (print (string-append "[" a "]"))
	     (p 'inc 1)
	     (p 'set-info (string-trim a))
	     (let ((file (string-append (number->string (hash a)) ".html")))
	       (format artist-p "<div onclick=\"showArtist('~a','~a');\">~a</div>\n"
		       (uri-encode-string a)
		       (hash a) a)	     
	       (call-with-output-file (string-append target-dir file)
		 (lambda (track-p)
		   (for-each
		    (lambda (t)
		      (format track-p  "<div onclick='queuetrack(~s,~s);'>~a</div>"
			      (uri-encode-string t) (uri-encode-string a) t ))
		    
		    (if (member a classical-composer-list)
			(mpc 'find 'composer a)
			(mpc 'find 'artist a))
		    
		    
		    )))))
	   
	   (sort artist-list (lambda (x y) (string-ci<? (sortable-bandname x)
							(sortable-bandname y)))))))
							
      (p 'finish)
      ;;(print "Syncing with the remote server")
      (sync target-dir "ghidorah@reactoweb.com:~/www/jukebox/data/")
      )))
    
