#!/usr/local/bin/gosh
;; -*- scheme -*-

(use file.util)
(use dbm)
(use dbm.gdbm)
(use gauche.collection)
(use srfi-13)
(use data.heap)

(define basedir "~/")

(define get-artist (lambda (x) (car (string-split x "/"))))

(define getdb
  (lambda ()
    (let  ((db (dbm-open <gdbm> :path (expand-path (format #f "~adb/~a.db" basedir 'playcount)) :rw-mode :read))
	   (H (make-hash-table 'string=?))
	   (i 0))      
      
      (dbm-for-each db 
		    (lambda (k v)
		      (let* ((artist (get-artist k))
			     (artist-count (hash-table-get H artist 0)))
			(hash-table-put! H artist (+ artist-count (string->number v))))))
      (dbm-close db)
      (let ((t10 (make-binary-heap :key car)))
	(hash-table-for-each H (lambda (k v) (binary-heap-push! t10 (cons v k))))  
	(until (binary-heap-empty? t10)
	       
	       (let* ((top (binary-heap-pop-max! t10)))
		 
		 (unless (member (cdr top) '("Compilations" "OST"))
			 (set! i (+ 1 i))
			 (format #t "~4@a ~4@a ~a\n" i (car top) (cdr top))
			 (flush)))
	       )))))


(define main 
  (lambda (args)
    (getdb)))
