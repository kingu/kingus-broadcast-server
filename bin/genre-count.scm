#!/usr/local/bin/gosh

(use kingu.taffy)
(use kingu.mpd)
(use kingu.mpc)
(use kingu.file)

(define main
  (lambda (args)
    (for-each
     (lambda(x)
       (format #t "~a: ~a\n" (car x)(cdr x)))
     (sort
      (map
       (lambda (g)
	 (cons (length (if (string=? "tracks" (cadr args))
			   (mpc 'find 'genre g)
			   (mpc 'list 'artist 'genre g))) g))
      (mpc 'list 'genre))
      > car))))
