#!/usr/local/bin/gosh

(use kingu.taffy)
(use kingu.mpd)
(use kingu.mpc)

(define main
  (lambda (args)
    (let* ((tag (cadr args))
	  (old (caddr args))
	  (new (cadddr args))
	  (files (map cdr (filter (lambda (x) (eq? 'file (car x))) (mpd (format #f "find ~a ~s" tag old))))))
      (for-each
       (lambda (x)
	 (print x)
	 (taffy-write! (string-append "/home/grpmpdp/music/" x )`((,tag ,new)))
	 (mpc-update-track x))
       files)
      
       )))
