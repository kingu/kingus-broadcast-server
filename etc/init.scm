#!/usr/local/bin/gosh

;; GRPMPDP startup 
;; Call this script from /etc/init.d to start/stop the system
;;
;; Args are:
;;
;;  stop   
;;   -- Stop grpmpdp (by killing pids from pidfiles and deleting them)
;;  start
;;   -- Start the subsystems and create pidfiles
;;  restart
;;   -- Execute stop, then start
;;  force-restart
;;   -- Ignore pid files, delete them and kill processes by name.  Then start.
;;   -- Use this option if you crashed or quit grpmpdp abnormaly and are now getting a
;;      "pidfiles already exists!" when you want to start.

(use file.util)
(use gauche.process)

(define username "grpmpdp")

(load (string-append "/home/" username "/.beholder/conf.scm"))
(sys-setenv "PATH" (string-append (sys-getenv "PATH") ":" bin-path))

(define main
  (lambda (args)
    (let ((action (cdr args)))
      (unless (= (sys-getuid) 0) 
	      (error "This program must be ran as the root user"))
      (when (null? action)
	    (error "Argument required (stop|start|restart|force-restart)"))	    
      ;(current-directory grpmpdp-basedir) 
      (case (string->symbol (car action))
	((stop) (stop))
	((start) (start))
	((restart) (stop)(start))
	(else (error (format #f "Argument invalid (~a). Valid args are: stop, start, restart\n")))))))	    

(define stop
  (lambda ()
    (stop-beholder)))

(define start
  (lambda ()
    (start-beholder)))

(define start-beholder
  (lambda ()
    (when (file-exists? "/var/run/beholder.pid")
	  (error "Beholder pid file already exists!"))
    (let ((beholder-process (run-process `(su -c ,(string-append bin-path "/beholder"),username))))
      (call-with-output-file "/var/run/beholder.pid"
	(lambda (p) (write (process-pid beholder-process) p))))))
    
(define get-pid-from-file 
  (lambda (file)
    (unless (file-exists? file)
	    (error "Pid file not found"))
    (string->number (file->string file))))

(define stop-process
  (lambda (process-name)
    (print process-name)
    (let ((pid (get-pid-from-file (string-append "/var/run/" process-name ".pid"))))
      (run-process `(kill ,pid) :wait #t))))

(define stop-beholder
  (lambda ()
    (stop-process "beholder")
    (delete-file "/var/run/beholder.pid")
    ))
