(use kingu.process)
(use srfi-13)
(use rfc.uri)

(define get-current-request
  (lambda ()
    (let ((po (read-from-string
	       (process->string
		"curl -k --max-time 120 https://kingu.reactoweb.com/getter/getrequest.php 2>/dev/null"))))
      (if (null? po)
	  (begin (=> "No request") #f)
	  (filter (lambda(x)
		    (and
		     (not (string=? "" x))
		     (or (#/^pl_.*/ x)
			 (file-exists? (path/file music-path x)))))
		  (map uri-decode-string po))))))

(define add-requested-tracks
  (lambda ()
    (let ((cr (get-current-request)))
      (=> "Remote Request: " cr)
      (unless (or (null? cr)
		  (eq? cr '("")))
	      (=> "adding requests")
	      (for-each
	       (lambda (x)
		 (if (#/^pl_.*/ x)
		     (add-playlist (cadr (string-split x #\_)))
		     (mpc 'add x)))
	       cr)))))

(define add-playlist
  (lambda (pl)
    (mpc 'load pl)))

(define jukebox
  (lambda (app)
    (add-requested-tracks)))

(define beholder-module-execute
  (lambda (ps cs app)    
    (guard (e [else
	       (let ((err (call-with-output-string
			   (lambda (s) (report-error s)))))
		 (=> "Error in module Jukebox" (~ e 'message))
		 (=> "Report" err))
	       ]) 
	   (jukebox app))))
