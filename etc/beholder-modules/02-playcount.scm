(define update-count
  (lambda (file countname)
    (let* ((db (dbm-open <gdbm> :path (format #f "/home/grpmpdp/db/~a.db" countname) :rw-mode :write))
	   (i  (dbm-get db file "0"))
	   (i+ (number->string (+ (string->number i) 1))))
      (=> "playcount" file i+)
      (unwind-protect
       (dbm-put! db file i+)
       (dbm-close db)))))

(define update-playcount (lambda (f) (update-count f 'playcount))) 

(define beholder-module-execute
  (lambda (ps cs app)
    (guard (e [else
	       (let ((err (call-with-output-string
			   (lambda (s) (report-error s)))))
		 (=> "Error in module update" (~ e 'message))
		 (=> "Report" err))])
	   (update-playcount (cdr (assoc 'file cs))))))
