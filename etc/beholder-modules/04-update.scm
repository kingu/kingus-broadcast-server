(use rfc.http)
(use srfi-11)
(use srfi-13)
(use gauche.process)
(use kingu.string)
(use data.queue)
(define fetch-assoc
  (lambda (k al)
    (let ((x (assoc k al)))
      (if x (cdr x) "Unknown"))))

(define get-next-track
  (lambda ()
    (let ((l (process->list "mpc -f \"%artist%\n%title%\" playlist")))
      (string-append 
       (list-ref l 2)
       "\n"
       (list-ref l 3)))))
  
(define get-playlist
  (lambda ()
    (string-join (map (lambda (x)
			(string-append "<li>" x "</li>"))
		      (mpc 'playlist)))))

(define get-queue
  (lambda (app)
    (string-join
     (map (lambda (x)
	    (format #f "<li>~a - ~a</li>" (~ x 'Artist)(~ x 'Title)))
	  (queue->list (hash-table-get app 'tracks))))))


(define set-cs-data!
  (lambda (data app)
    (set-cs-data-to-webpage! data app)
    ))

  
(define set-cs-data-to-webpage!
  (let ((setter-server "kingu.reactoweb.com")
	(setter-request-uri "/setter/") )
    (lambda (data app)
      (let-values (((code headers body) (http-post
					 setter-server
					 setter-request-uri
					 `(("current_title"  ,(fetch-assoc 'Title data))
					   ("current_artist" ,(fetch-assoc 'Artist data))
					   ("current_album"  ,(fetch-assoc 'Album data))
					   ("current_file"   ,(fetch-assoc 'file data))
					   ("current_genre"  ,(fetch-assoc 'Genre data))
					   ("playlist"       ,(get-playlist))
					   ("queue"          ,(get-queue app))
					   ("next_tracks"    ,(get-next-track))
					   ) 
					 :user-agent "Beholder"
					 :secure #t
					 :auth-password  web-password
					 :auth-user web-user
					 )))
	
	(if (string=? "200" code)
	    (=> "cs data sended to server sucesfully" setter-server code body )
	    (begin
	      (=> "An error occur during transmission of cs data to server" code headers body )
	      (error "Update failed"))
	    )))))


(define beholder-module-execute
  (lambda (ps cs app)

    (guard (e [else
	       (let ((err (call-with-output-string
			   (lambda (s) (report-error s)))))
		 (=> "Error in module update" (~ e 'message))
		 (=> "Report" err))

	       
	       ]) 
	   (set-cs-data! cs app)
	   )))
