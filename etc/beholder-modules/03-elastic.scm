
(use rfc.http)
(use srfi-13)
(use data.queue)
(use kingu.list)
(use kingu.string)

(define file->track 
  (lambda (file)
    (let ((trk (make <track> :file file))
	  (tif (mpd (format #f "find file ~s" file))))
      (for-each 
       (lambda (x) (slot-set! trk (car x)(cdr x)))
       tif)
      trk)))


(define get-autodj-genres-list
  (lambda ()
    (read-from-string
     (remove-from-string
      (process->string "curl -k --max-time 120 https://kingu.reactoweb.com/getter/getautodj.php 2>/dev/null") #\,))))
	    
(define make-index
  (lambda (genre)
    (let* ((a (map cdr (mpd (format #f "list artist  genre ~s" genre))))
	   (ba (list))
	   (sa (list)))
      (for-each
       (lambda (x)
	 (let ((i (string->number (cdar (mpd (format #f "count genre ~s artist ~s" genre x))))))
	   (if (<= i 5)
	       (set! sa (cons
			 (map cdr (filter (lambda (x)
					    (eq? 'file (car x)))
					  (mpd (format #f "find artist ~s genre ~s" x genre))))
			 sa))
	       (set! ba (cons
			 (map cdr (filter (lambda (x)
					    (eq? 'file (car x)))
					  (mpd (format #f "find artist ~s genre ~s" x genre))))
			 ba)))))
       a)
      (call-with-output-file (format #f "/home/grpmpdp/idx/~a.idx" genre)
	  (lambda (p)
	    (write (filter (lambda (x) (not (null? x))) (cons (flatten sa) ba)) p)) ))))

(define genre->playlist
  (lambda (genre tpp)
     (unless (file-exists? (format #f "/home/grpmpdp/idx/~a.idx" genre))
	      (make-index genre))
     (let* ((fl (call-with-input-file (format #f "/home/grpmpdp/idx/~a.idx" genre)
		  (lambda (p) (read p))))
	    (tpl (max 1 (div tpp (length fl))))
	    (pl (take* (shuffle (flatten (map (lambda (l) (take* (shuffle l) tpl)) fl))) tpp))) 
       pl)))

  ;(read-from-string (file->string (format #f "/home/grpmpdp/idx/~a.idx" genre))))


;; (define make-playlist-from-genre
;;   (lambda (genre)
;;     (parse-mpd-result-list (mpd (format #f "find genre ~s" genre)))))

(define make-playlist-from-genres-list
  (lambda (n g)
    (map file->track (take* (shuffle (flatten (map (lambda (x) (genre->playlist x n)) g))) n))))

(define make-playlist-from-all-files
  (lambda (n)
    (map (lambda (x) (file->track (cdr x))) (subseq (shuffle (mpd "listall")) 0 n))))


(define get-tracks
  (lambda (n g)
    (if (and g
	     (not (null? g)))
	(make-playlist-from-genres-list n g)
	(make-playlist-from-all-files n))
    ))


(define elastic
  (lambda (app)
    (let ((glf (get-autodj-genres-list))
	  (gla (hash-table-get app 'genres #f)))
      (unless (list? glf)
	      (set! glf '()))
      (unless (and (hash-table-exists? app 'tracks)
		   (equal? glf gla))
	      (=> "Creating new tracklist" glf)
	      (hash-table-put! app 'genres glf)
	      (hash-table-put! app 'tracks (list->queue (get-tracks 25 glf))))
      (let ((Q (hash-table-get app 'tracks)))
	(=> "queue length" (queue-length  Q))
;;	(=> (map (lambda (x) (~ x 'Genre)) (queue->list Q)))
	(when (<= (length (mpc "playlist")) 2)
	      (mpc 'add (~ (dequeue! Q) 'file)))
	(when (< (queue-length  Q) 20)
	      (=> "Adding more tracks to the queue")
	      (for-each
	       (lambda (x) (enqueue! Q x))
	       (get-tracks 10 glf))))))
  )

(define beholder-module-execute
  (lambda (ps cs app)    
    (guard (e [else
	       (let ((err (call-with-output-string
			   (lambda (s) (report-error s)))))
		 (=> "Error in module elastic" (~ e 'message))
		 (=> "Report" err))
	       ]) 
	   (elastic app))))

