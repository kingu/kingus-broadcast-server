(define beholder-module-execute
  (lambda (subsys cs app)
    (=> "subsys" subsys)
    (=> "cs" cs) 
    (=> "app" (hash-table->alist app))
    (when (any-member? '("player") subsys)
	  (=> "player ok"))))
